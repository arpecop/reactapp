import React from "react";
import { withStyles, SnackbarContent as Snack } from "material-ui";
import Button from "components/CustomButtons/Button.jsx";

//import { Close } from "@material-ui/icons";
// nodejs library to set properties for components
import PropTypes from "prop-types";

import snackbarContentStyle from "../../assets/jss/material-kit-react/components/snackbarContentStyle.jsx";

class SnackbarContent extends React.Component {
  constructor(props) {
    super(props);
    this.closeAlert = this.closeAlert.bind(this);
    const { classes, message, color, close, icon } = props;
    var action = [];
    if (close) {
      action = [
        <Button
          style={{ float: 'right' }}
          key="close"
          aria-label="Close"
          color="primary"
          onClick={this.closeAlert}
        >
          {this.props.closeText}
        </Button>
      ];
    }
    this.state = {
      alert: (
        <Snack
          message={
            <div>

              {message}

              {icon ? (
                <props.icon />
              ) : null}
              {close !== undefined ? action : null}
            </div>
          }
          classes={{
            root: classes.root + " " + classes[color],
            message: classes.message + " " + classes.container
          }}
        />
      )
    };
  }
  closeAlert() {
    this.setState({ alert: null });
  }
  render() {
    return this.state.alert;
  }
}

SnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.node.isRequired,
  color: PropTypes.oneOf(["info", "success", "warning", "danger", "primary"]),
  close: PropTypes.bool,
  icon: PropTypes.func
};

export default withStyles(snackbarContentStyle)(SnackbarContent);
