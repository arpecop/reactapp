import React, { Component } from 'react'
import { Redirect } from 'react-router'
import GoogleMapReact from 'google-map-react'
import Badge from 'components/Badge/Badge.jsx'
import { getCached, getsess, setsess, setsessRemote, get } from './api/index.js'
// material-ui components
import withStyles from 'material-ui/styles/withStyles'

import { Helmet } from 'react-helmet'

import profilePageStyle from 'assets/jss/material-kit-react/views/profilePage.jsx'

class Marker extends Component {
    render() {
        return (
            <div
                style={{
                    minWidth: 50,
                    minHeight: 50,
                    cursor: 'pointer',
                    position: 'absolute',
                    top: -25,
                    left: -25,
                }}
            >
                <Badge color={this.props.color}>{this.props.text}</Badge>
            </div>
        )
    }
}

class Corona extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cats: [],
            subcats: [],
            counties: [],
            center: { lat: 42.8742212, lng: 25.3186837 },
            zoom: 6.5,
            dimensions: {},
            width: 0,
            height: 0,
            selected: null,
            session: { tosaccepted: true, selected: false },
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
        this._onClick = this._onClick.bind(this)
        this._login = this._login.bind(this)
    }
    async componentDidMount() {
        const date = new Date()
        const tosaccepted = await getsess('tosaccepted')
        const selected = await getsess('selected')
        const cats = await get('cats')
        const counties = await getCached('county')
        if (!tosaccepted) {
            setsess('tosaccepted', { '1': date.setHours(date.getHours()) })
        }

        this.setState({
            cats: cats.rows,
            counties: counties.rows,
            session: { tosaccepted, selected },
        })
        this.updateWindowDimensions()
        window.addEventListener('resize', this.updateWindowDimensions)
    }

    updateWindowDimensions() {
        const dimensionz = {
            width: window.innerWidth,
            height: window.innerHeight,
            availHeight: window.screen.height * window.devicePixelRatio,
        }
        this.setState(dimensionz)

        setsess('dimensions', dimensionz)
    }

    _onClick(obj) {
        const subcats = this.state.counties.filter(function(item) {
            if (item.key === obj && item.key !== item.id) {
                return item
            }
        })

        const selected = this.state.counties.filter(function(item) {
            if (item.id === obj) {
                return item
            }
        })

        this.setState({
            subcats: subcats.length > 1 ? subcats : this.state.subcats,
            selected: selected[0],
        })
    }
    async _login(obj) {
        await setsess('selected', this.state.selected.value)
        setsessRemote('selected', this.state.selected.value)
        this.setState({
            session: { tosaccepted: true, selected: this.state.selected.value },
        })
    }
    render() {
        const googleMapIndex = (
            <GoogleMapReact
                onChildClick={this._onClick}
                bootstrapURLKeys={{
                    key: 'AIzaSyAJdMUyuQiG2DEHgGG3Tvebb9-BzR0JXwE',
                }}
                defaultCenter={this.state.center}
                defaultZoom={this.state.zoom}
            >
                {this.state.cats.map(prop => (
                    <Marker
                        key={prop.key}
                        lat={prop.value.location.lat}
                        lng={prop.value.location.lng}
                        text={prop.value.text}
                        color="rose"
                    />
                ))}

                {this.state.subcats.map(prop => (
                    <Marker
                        key={prop.id}
                        lat={prop.value.location.lat}
                        lng={prop.value.location.lng}
                        text={prop.value.text}
                        color="primary"
                    />
                ))}
            </GoogleMapReact>
        )

        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Corona Bulgaria</title>
                    <meta
                        name="description"
                        content=" Варна, София, Пловдив, Добрич , Русе,Видин, Велико Търново Vijdam te kat"
                    />
                    <link rel="canonical" href="http://mysite.com/example" />
                </Helmet>

                <div
                    className="google-map"
                    style={{
                        position: 'absolute',
                        width: '100%',
                        height: this.state.height,
                    }}
                >
                    {this.state.session.selected && this.state.height > 1 ? (
                        <Redirect
                            to={{
                                pathname: '/' + this.state.session.selected.id,
                            }}
                        />
                    ) : (
                        googleMapIndex
                    )}
                </div>
            </div>
        )
    }
}

export default withStyles(profilePageStyle)(Corona)
