import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import Badge from 'components/Badge/Badge.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Tags from 'react-tagging-input';
import Navigation from './Components/Navigation';

import { getsess, setsess, put } from './api';
import { Redirect } from 'react-router';
import './src/tags.css';

class Marker extends Component {
  render() {
    return (
      <div>
        {this.props.selectedTags[0] ? (
          <Badge color="rose" size="sm">
            {this.props.selectedTags.join(',  ')}
          </Badge>
        ) : (
          <div>
            <div className="pin bounce" />
            <div className="pulse" />
          </div>
        )}
      </div>
    );
  }
}

export default class AddItem extends React.Component {
  constructor(props) {
    super(props);
    const lng = Number(this.props.match.params.lng);
    const lat = Number(this.props.match.params.lat);
    this.state = {
      lng,
      lat,
      approx: lng * lat,

      selectedTags: [],
      event: {},
      published: false,
    };

    this.onTagAdded = this.onTagAdded.bind(this);
    this.onTagRemoved = this.onTagRemoved.bind(this);
    //this._Publish = this._Publish.bind(this);
    this.publish = this.publish.bind(this);
  }

  _prePublish(tag) {
    this.setState({ selectedTags: [tag] });
  }

  onTagAdded(tag) {
    this.setState({
      selectedTags: [...this.state.selectedTags, tag],
    });
  }
  onTagRemoved(tag, index) {
    this.setState({
      selectedTags: this.state.selectedTags.filter((tag, i) => i !== index),
    });
  }

  async publish() {
    const date = new Date();
    const accurateLocation = await getsess('selected');
    await setsess('tosaccepted', { '1': date.setHours(date.getHours() + 1) });
    await put({
      _id: new Date().getTime() + '_event',
      ...accurateLocation,
      tags: this.state.selectedTags,
      type: 'event',
      date: new Date(),
      tag: this.state.selectedTags[0],
      location: { lat: this.state.lat, lng: this.state.lng },
    });
    this.setState({ published: accurateLocation.id });
  }

  async _Publish() {
    // const accurateLocation = await get("search?start_key=" + this.state.approx + "&limit=1");
    const date = new Date();
    const curDate = date.setHours(date.getHours());
    const lastPublish = await getsess('tosaccepted')['1'];

    if (lastPublish < curDate) {
      this.publish();
    } else {
      alert('вече е публикувано от тази локация');
    }
  }

  render() {
    return (
      <div>
        <div
          className="addevent"
          style={{
            position: 'fixed',
            bottom: 0,
            left: 0,
            zIndex: 2,
            textAlign: 'center',
            width: '100%',
            height: '50%',
          }}
        >
          <div>
            <Tags
              placeholder="Добави ключови думи"
              tags={this.state.selectedTags}
              uniqueTags={true}
              removeTagIcon={
                <div
                  style={{
                    width: 22,
                    height: 22,
                    color: '#FFF',
                    fontSize: 24,
                    cursor: 'pointer',
                    position: 'absolute',
                    marginTop: -25,
                  }}
                >
                  x
                </div>
              }
              onAdded={this.onTagAdded}
              onRemoved={this.onTagRemoved}
            />
          </div>

          <div>{this.state.published ? <Redirect push to={'/' + this.state.published} /> : ''}</div>
        </div>
        <div
          style={{
            backgroundColor: '#FFF',
            position: 'fixed',
            top: 0,
            left: 0,

            width: '100%',
            height: '100%',
          }}
        >
          <GoogleMapReact
            bootstrapURLKeys={{
              key: 'AIzaSyCAZIPH1DMiU76SUSxSsHTUy72NAkMaDHg',
            }}
            onChildClick={this._onChildClick}
            defaultCenter={{ lng: this.state.lng, lat: this.state.lat }}
            defaultZoom={17}
          >
            <Marker lat={this.state.lat + 0.0010111} lng={this.state.lng} selectedTags={this.state.selectedTags} />
          </GoogleMapReact>
        </div>
        <Navigation position="top" backButton="back">
          {this.state.selectedTags[0] ? (
            <Button onClick={() => this._Publish()} color="primary" size="md">
              Публикувай
            </Button>
          ) : (
            <div style={{ padding: 15 }}> Добави събитие на пътя</div>
          )}
        </Navigation>
      </div>
    );
  }
}
