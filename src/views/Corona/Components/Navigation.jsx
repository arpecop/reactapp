import React from "react";

import { Redirect } from "react-router";
import Refresh from "@material-ui/icons/Refresh";

import ArrowBackIos from "@material-ui/icons/ArrowBack";
//import RenderToLayer from "material-ui/internal/RenderToLayer";

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    };
  }
  _getBack() {
    this.setState({ redirect: true });
  }
  render() {
    const { children } = this.props;
    const position =
      this.props.position === "top"
        ? { top: 0, borderBottom: "1px solid #e5e5e5" }
        : { bottom: 0, borderTop: "1px solid #e5e5e5" };
    const hidden = this.props.hidden
      ? { display: "none" }
      : { display: "block" };
    const iconStyle = {
      position: "absolute",
      left: 8,
      top: 8,
      width: 40,
      height: 40,
      cursor: "pointer"
    };
    let LeftButton;
    if (this.props.backButton && this.props.backButton === "refresh") {
      LeftButton = (
        <Refresh onClick={this._getBack.bind(this)} style={iconStyle} />
      );
    } else if (this.props.backButton && this.props.backButton === "back") {
      LeftButton = (
        <ArrowBackIos onClick={this._getBack.bind(this)} style={iconStyle} />
      );
    } else {
      LeftButton = <div />;
    }
    return (
      <div
        style={{
          position: "fixed",
          ...position,
          ...hidden,
          width: "100%",
          backgroundColor: this.props.color ? this.props.color : "#FFF",
          height: 57,
          color: "#3C4858",
          padding: 0,
          overflow: "hidden",
          textAlign: this.props.textAlign ? this.props.textAlign : "center",
          zIndex: 100,
          fontWeight: "bolder"
        }}
      >
        {LeftButton}
        {this.state.redirect ? <Redirect push to={"/"} /> : ""}

        {this.props.title ? (
          <div style={{ padding: 15 }}>{this.props.title}</div>
        ) : (
          ""
        )}
        {children}
      </div>
    );
  }
}

export default Navigation;
