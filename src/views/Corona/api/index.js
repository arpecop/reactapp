import axios from 'axios'

export async function get(url) {
    const src = await fetch(
        'https://db.arpecop.xyz/vijdam/_design/api/_view/' + url
    )
    return await src.json()
}

export async function getCached(url) {
    const cached = localStorage.getItem(url)
    if (!cached) {
        const src = await fetch(
            'https://db.arpecop.xyz/vijdam/_design/api/_view/' + url
        )
        const content = await src.json()
        localStorage.setItem(url, JSON.stringify(content))
        return content
    } else {
        return JSON.parse(cached)
    }
}

export async function getid(url) {
    const src = await fetch('https://db.arpecop.xyz/vijdam/' + url)
    return await src.json()
}

export async function fbPublicSearch(term) {
    const src = await fetch(
        'https://graph.facebook.com/search?access_token=2006734596064475|xVl0xzLlRHZc-Gv0lpQ0zGKmz2Yq=' +
            term +
            '&type=OBJECT_TYPE'
    )
    return await src.json()
}

export function getsess(key) {
    const item = localStorage.getItem(key)
    if (item) {
        return JSON.parse(item)
    } else {
        return false
    }
}

export function setsess(key, json) {
    localStorage.setItem(key, JSON.stringify(json))
    return {}
}

export function setsessRemote(key, json) {
    return new Promise((resolve, reject) => {
        axios('//sessionless.herokuapp.com/' + key, {
            method: 'post',
            data: json,
            withCredentials: true,
        })
            .then(jsonx => {
                resolve({})
            })
            .catch(err => {
                resolve({})
            })
    })
}

export function put(json) {
    return new Promise((resolve, reject) => {
        axios('https:https://db.arpecop.xyz/vijdam', {
            method: 'post',
            data: json,
            headers: {
                'content-type': 'application/json',
            },
        })
            .then(jsonx => {
                resolve({})
            })
            .catch(err => {
                resolve({})
            })
    })
}

export function find(json) {
    return new Promise((resolve, reject) => {
        axios('https://db.arpecop.xyz/vijdam/_find', {
            method: 'post',
            data: { selector: json },
            headers: {
                'content-type': 'application/json',
            },
        })
            .then(jsonx => {
                resolve(jsonx.data)
            })
            .catch(err => {
                resolve({})
            })
    })
}
