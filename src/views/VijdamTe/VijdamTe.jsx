import React, { Component } from 'react'
import { Redirect } from 'react-router'
import GoogleMapReact from 'google-map-react'
import Badge from 'components/Badge/Badge.jsx'
import { getCached, getsess, setsess, setsessRemote } from './api/index.js'
// material-ui components
import withStyles from 'material-ui/styles/withStyles'

import TouchApp from '@material-ui/icons/TouchApp'
import LocationOn from '@material-ui/icons/LocationOn'
import SnackbarContent from '../../components/Snackbar/SnackbarContent.jsx'
import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile,
} from 'react-device-detect'

import { Helmet } from 'react-helmet'
import Navigation from './Components/Navigation'
import Button from 'components/CustomButtons/Button.jsx'
// import List from "@material-ui/icons/List";

import profilePageStyle from 'assets/jss/material-kit-react/views/profilePage.jsx'

// class Tech extends React.Component {

class Marker extends Component {
    render() {
        return (
            <div
                style={{
                    minWidth: 50,
                    minHeight: 50,
                    cursor: 'pointer',
                    position: 'absolute',
                    top: -25,
                    left: -25,
                }}
            >
                <Badge color={this.props.color}>{this.props.text}</Badge>
            </div>
        )
    }
}

class VijdamTe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cats: [],
            subcats: [],
            counties: [],
            center: { lat: 42.8742212, lng: 25.3186837 },
            zoom: 6.5,
            dimensions: {},
            width: 0,
            height: 0,
            selected: null,
            session: { tosaccepted: true, selected: false },
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
        this._onClick = this._onClick.bind(this)
        this._login = this._login.bind(this)
    }
    async componentDidMount() {
        const date = new Date()
        const tosaccepted = await getsess('tosaccepted')
        const selected = await getsess('selected')
        const cats = await getCached('cats')
        const counties = await getCached('county')
        if (!tosaccepted) {
            setsess('tosaccepted', { '1': date.setHours(date.getHours()) })
        }

        this.setState({
            cats: cats.rows,
            counties: counties.rows,
            session: { tosaccepted, selected },
        })
        this.updateWindowDimensions()
        window.addEventListener('resize', this.updateWindowDimensions)
    }

    updateWindowDimensions() {
        const dimensionz = {
            width: window.innerWidth,
            height: window.innerHeight,
            availHeight: window.screen.height * window.devicePixelRatio,
        }
        this.setState(dimensionz)

        setsess('dimensions', dimensionz)
    }

    _onClick(obj) {
        const subcats = this.state.counties.filter(function(item) {
            if (item.key === obj && item.key !== item.id) {
                return item
            }
        })

        const selected = this.state.counties.filter(function(item) {
            if (item.id === obj) {
                return item
            }
        })

        this.setState({
            subcats: subcats.length > 1 ? subcats : this.state.subcats,
            selected: selected[0],
        })
    }
    async _login(obj) {
        await setsess('selected', this.state.selected.value)
        setsessRemote('selected', this.state.selected.value)
        this.setState({
            session: { tosaccepted: true, selected: this.state.selected.value },
        })
    }
    render() {
        const bothPlatforms = (
            <div>
                {' '}
                Приложението е разработено като алтернатива на популярните
                Facebook групи, като не носи отговорност за публикациите от
                потребителите.
                <br />
                Лични данни не се изискват съответно не се събират или споделят
                с трети лица.
                <br />
            </div>
        )
        const acceptTos = (
            <div
                id="acceptos"
                style={{
                    position: 'absolute',
                    top: 20,
                    left: 20,
                    right: 20,
                    zIndex: 111,
                }}
            >
                <SnackbarContent
                    message={
                        <span style={{ fontSize: 17 }}>
                            <BrowserView device={isBrowser}>
                                {bothPlatforms}
                                <strong>
                                    {' '}
                                    оптимизирано и създадено за мобилен телефон
                                    (Anroid или Apple) с геолокация
                                </strong>
                            </BrowserView>
                            <MobileView device={isMobile}>
                                {bothPlatforms}
                                използвайте два пръста за да скролирате и
                                увеличите картата.
                            </MobileView>
                        </span>
                    }
                    close
                    closeText="разбрах"
                    color="success"
                />
            </div>
        )

        const googleMapIndex = (
            <GoogleMapReact
                onChildClick={this._onClick}
                bootstrapURLKeys={{
                    key: 'AIzaSyCAZIPH1DMiU76SUSxSsHTUy72NAkMaDHg',
                }}
                defaultCenter={this.state.center}
                defaultZoom={this.state.zoom}
            >
                {this.state.cats.map(prop => (
                    <Marker
                        key={prop.key}
                        lat={prop.value.location.lat}
                        lng={prop.value.location.lng}
                        text={prop.value.text}
                        color="rose"
                    />
                ))}

                {this.state.subcats.map(prop => (
                    <Marker
                        key={prop.id}
                        lat={prop.value.location.lat}
                        lng={prop.value.location.lng}
                        text={prop.value.text}
                        color="primary"
                    />
                ))}
            </GoogleMapReact>
        )

        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Vijdam Te Kat</title>
                    <meta
                        name="description"
                        content="Виждам те кат Варна, София, Пловдив, Добрич , Русе,Видин, Велико Търново Vijdam te kat"
                    />
                    <link rel="canonical" href="http://mysite.com/example" />
                </Helmet>
                {this.state.session.tosaccepted ? null : acceptTos}
                <div
                    className="google-map"
                    style={{
                        position: 'absolute',
                        width: '100%',
                        height: this.state.height - 114,
                        marginTop: 57,
                    }}
                >
                    {this.state.session.selected && this.state.height > 1 ? (
                        <Redirect
                            to={{
                                pathname: '/' + this.state.session.selected.id,
                            }}
                        />
                    ) : (
                        googleMapIndex
                    )}
                </div>
                <Navigation position="top">
                    <img
                        src={
                            '//gitlab.com/arpecop/reactapp/raw/master/public/fb/icon.png'
                        }
                        style={{ width: 57 }}
                        alt=""
                    />
                </Navigation>

                <Navigation position="bottom">
                    {this.state.selected ? (
                        <Button color="primary" size="sm" onClick={this._login}>
                            <LocationOn />{' '}
                            {this.state.selected
                                ? 'Потвърди ' + this.state.selected.value.text
                                : ''}
                        </Button>
                    ) : (
                        <div style={{ padding: 15 }}>
                            <TouchApp
                                style={{ position: 'absolute', left: 5 }}
                            />
                            Избери област и след тованаселено място
                        </div>
                    )}
                </Navigation>
            </div>
        )
    }
}

export default withStyles(profilePageStyle)(VijdamTe)
