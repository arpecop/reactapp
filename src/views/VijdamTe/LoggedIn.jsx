import React, { Component } from 'react';

import { getsess, get, setsess, setsessRemote } from './api/index.js';
import Geolocation from 'react-geolocation';

import Badge from 'components/Badge/Badge.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Navigation from './Components/Navigation';
import IconButton from 'components/CustomButtons/IconButton.jsx';

import GoogleMapReact from 'google-map-react';
import TimeAgo from 'react-timeago';
import axios from 'axios';
//import AddToHomeScreen from '@rajatsehgal/add-to-home-screen/AddToHomeScreen';
import { Redirect } from 'react-router';
import PlusIcon from '@material-ui/icons/Add';
//icons
import ListIcon from '@material-ui/icons/List';

import MapIcon from '@material-ui/icons/Map';
import NotificationsIcon from '@material-ui/icons/Notifications';

function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

class Marker extends Component {
  render() {
    return (
      <div>
        <div className="pin bounce" />
        <div
          className="pinInfo"
          style={{
            position: 'absolute',
            top: -20,
            left: -15,
            minWidth: 150,
            color: '#FFF',
            paddingLeft: 5,
            backgroundColor: '#4080ff',
            borderRadius: 10,
          }}
        >
          <TimeAgo date={this.props.date} />: <br />
          {this.props.tag}
        </div>
      </div>
    );
  }
}

class LoggedIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      center: { lat: 42.8742212, lng: 25.3186837 },
      width: null,
      height: null,
      nicerButton: false,
      seen: false,
      addicon: 'add',
      events: { rows: [] },
      isSubscriber: true,
      isMapView: false,
      isListView: true,
      session: { selected: {} },
    };
    this.notificationsRequest = this.notificationsRequest.bind(this);
    this._changeView = this._changeView.bind(this);
  }
  async componentDidMount() {
    const id = this.props.match.params.id;
    const county = await getsess('county');
    const selected = county.rows.filter(function(item) {
      return item.id === id ? item : null;
    })[0].value;
    const dimensions = await getsess('dimensions');
    const subscriber = await getsess('subscriber');
    const events = await get('events?key="' + selected.id + '"&limit=100&reduce=false&descending=true');

    //seen
    const total = await get('events?key="' + selected.id + '"&reduce=true');
    const seenSess = await getsess('seen');
    const totalItems = total.rows[0] ? total.rows[0].value : 0;
    const seen = seenSess.seen ? totalItems - seenSess.seen : totalItems;
    await setsess('seen', { seen: totalItems });
    await setsessRemote('seen', { seen: totalItems });
    //news
    await this.setState({
      session: { selected },
      isSubscriber: subscriber ? true : false,
      width: dimensions.width,

      events,
      new: seen,
      height: dimensions.height,
    });

    setTimeout(
      function() {
        this.setState({ nicerButton: true });
      }.bind(this),
      3000,
    );
  }

  async notificationsRequest() {
    // const selected = await getsess("selected");
    this.setState({ isSubscriber: true });
    Notification.requestPermission().then(function(result) {
      if (result === 'denied') {
      }
      if (result === 'default') {
      }
      if (result === 'granted') {
        setsess('subscriber', { subscriber: true });
        navigator.serviceWorker.ready.then(registration => {
          const vapidPublicKey =
            'BOaP9bM3L_LUQpI3L33AUABLWSDpmAzXIobaOmwOWfOC_8RCaLqShrkaBNl4oDGq6SnHpn2LtQQUWZB4kOEpVi8';
          const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
          registration.pushManager
            .subscribe({
              userVisibleOnly: true, //Always display notifications
              applicationServerKey: convertedVapidKey,
            })
            .then(subscription => axios.post('https://sessionless.herokuapp.com/register/vijdam', subscription))
            .catch(err => console.error('Push subscription error: ', err));
        });
      }
      // Do something with the granted permission.
    });
  }
  _changeView(selectedView) {
    this.setState({
      isMapView: selectedView === 'map' ? true : false,
      isListView: selectedView === 'list' ? true : false,
    });
  }

  render() {
    const MapView =
      this.state.session.selected.location && this.state.isMapView ? (
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyCAZIPH1DMiU76SUSxSsHTUy72NAkMaDHg' }}
          defaultCenter={this.state.session.selected.location}
          defaultZoom={14}
          hover="false"
        >
          {this.state.events.rows.map(prop => (
            <Marker
              key={prop.id}
              lat={prop.value.location.lat}
              lng={prop.value.location.lng}
              tag={prop.value.tag}
              date={prop.value.date}
            />
          ))}
        </GoogleMapReact>
      ) : (
        ''
      );

    const ListView =
      this.state.events.rows && !this.state.isMapView ? (
        <div style={{ padding: 15 }}>
          {' '}
          {this.state.events.rows.map(prop => (
            <div
              style={{
                border: '2px solid #f5f2f0',
                backgroundColor: '#FFF',
                marginBottom: 10,
              }}
              key={prop.id}
            >
              <div style={{ padding: 10, fontSize: '0.9rem' }}>
                {prop.value.tags.map((tag, i) => (
                  <Badge color="rose" key={i}>
                    {tag}
                  </Badge>
                ))}
              </div>
              <div
                style={{
                  backgroundColor: '#f5f2f0',
                  fontSize: '0.8rem',
                  textAlign: 'right',
                  paddingRight: 5,
                }}
              >
                {' '}
                <TimeAgo date={prop.value.date} />
              </div>
            </div>
          ))}
        </div>
      ) : (
        ''
      );

    return (
      <div>
        <Navigation
          position="top"
          title={
            <div>
              {this.state.session.selected.text} днес
              {this.state.new > 0 ? <Badge color="rose">{this.state.new}</Badge> : ''}
            </div>
          }
          backButton="refresh"
        />

        <div
          className="google-map"
          style={{
            height: this.state.height ? this.state.height - 114 : '100%',
            width: this.state.width - 57,
            top: 57,
            left: 57,
            position: 'fixed',
            overflow: 'auto',
          }}
        >
          {MapView} {ListView}
        </div>

        <div style={{ position: 'fixed', bottom: 5, left: 5, zIndex: 101 }}>
          <Geolocation
            lazy
            render={({
              fetchingPosition,
              position: { coords: { latitude, longitude } = {} } = {},
              error,
              getCurrentPosition,
            }) => {
              var AddButton = this.state.nicerButton ? (
                <IconButton color="primary" onClick={getCurrentPosition}>
                  <PlusIcon />
                </IconButton>
              ) : (
                <Button color="primary" onClick={getCurrentPosition} size="sm">
                  {' '}
                  <PlusIcon />
                  Добави събитие
                </Button>
              );
              return (
                <div>
                  {latitude && longitude ? <Redirect push to={'/add/' + latitude + '/' + longitude} /> : ''}
                  {AddButton}
                </div>
              );
            }}
          />
        </div>
        <Navigation position="bottom" color="#FFF" textAlign="right">
          {!this.state.isSubscriber ? (
            <Button color="rose" size="sm" onClick={this.notificationsRequest}>
              {' '}
              <NotificationsIcon />
              Абонирай се за известия
            </Button>
          ) : (
            <div />
          )}
        </Navigation>
        <div
          className="sideBar"
          style={{
            width: 57,

            borderLeft: '1px solid #e5e5e5',
            height: this.state.height ? this.state.height - 112 : '100%',
            top: 56,
            left: 0,
            position: 'fixed',
            overflow: 'hidden',
            backgroundColor: '#FFF',
            zIndex: 101,
          }}
        >
          <div className={this.state.isListView + 'icon'}>
            <ListIcon style={{ width: 35, height: 35 }} onClick={() => this._changeView('list')} />
          </div>
          <div className={this.state.isMapView + 'icon'}>
            <MapIcon style={{ width: 35, height: 35 }} onClick={() => this._changeView('map')} />
          </div>
        </div>
      </div>
    );
  }
}

/*
      <Button color="rose" size="sm" href="http://patreon.com/vijdam">
            {" "}
            <i className="material-icons">money</i>
            Помогни на проекта
          </Button>

          */

export default LoggedIn;
