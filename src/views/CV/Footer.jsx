import React from 'react';
// nodejs library to set properties for components
import PropTypes from 'prop-types';
// nodejs library that concatenates classes
import classNames from 'classnames';
import { List, withStyles } from 'material-ui';

// @material-ui/icons

import footerStyle from 'assets/jss/material-kit-react/components/footerStyle.jsx';

function Footer({ ...props }) {
  const { classes, whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont,
  });

  return (
    <footer className={footerClasses}>
      <div className={classes.container}>
        <div className={classes.left}>
          <List className={classes.list} />
        </div>
        <div
          className={classes.right}
          style={{ textAlign: 'center', fontSize: '70%', lineHeight: '12px' }}
        >
          &copy; {1900 + new Date().getYear()} , I built this site with React
          components and a JSON Resume Schema. The full source code can be found
          in my Github repo.
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  whiteFont: PropTypes.bool,
};

export default withStyles(footerStyle)(Footer);
