import React, { Component } from 'react'

import classNames from 'classnames'

import withStyles from 'material-ui/styles/withStyles'
import { request } from 'graphql-request'
import { Helmet } from 'react-helmet'

import Badge from 'components/Badge/Badge.jsx'
import GridContainer from 'components/Grid/GridContainer.jsx'

import IconButton from 'components/CustomButtons/IconButton.jsx'
import Switch from 'react-switch'
import Button from 'components/CustomButtons/Button.jsx'

// import List from "@material-ui/icons/List";

import Parallax from '../../components/Parallax/Parallax.jsx'
import Footer from './Footer.jsx'

import profilePageStyle from 'assets/jss/material-kit-react/views/profilePage.jsx'

import SnackbarContent from '../../components/Snackbar/SnackbarContent.jsx'

async function graph(json) {
    return new Promise((resolve, reject) => {
        request(
            'https://graphqldbone.herokuapp.com/graphql',
            json
        ).then((data) => resolve(data))
    })
}

const techs = {
    jobs: [
        {
            title: 'Facebook App Developer  ,Ringful',
            date: {
                from: new Date('September 17, 2008 03:24:00'),
                to: new Date('December 17, 2008 03:24:00'),
            },
            description: 'Facebook API integration, VOIP, Jboss  ',
        },

        {
            title: 'Senior Web Developer,  Agency Venera',
            date: {
                from: new Date('November 17, 2008 03:24:00'),
                to: new Date('Juny 17, 2009 03:24:00'),
            },
            description:
                'At SOAP I was required to carry out full stack duties across a broad range of projects including php/MySQL with front end technologies ranging from html/css to Flash.',
        },

        {
            title: 'Print Design tourist Newspaper, Ballerman am Balkan',
            date: {
                from: new Date('December 17, 2009 03:24:00'),
                to: new Date('April 17, 2010 03:24:00'),
            },
            description:
                'Responsible for the brand   weekly newspaper design, PHP website',
        },

        {
            title: 'Web Developer, Golgstrand Reisen',
            date: {
                from: new Date('July 17, 2012 03:24:00'),
                to: new Date('September 17, 2013 03:24:00'),
            },
            description:
                'Gathering data  Organizing and structuring the plan Translating all related documents',
        },
        {
            title: 'FullStack Cloud Consultant, dMedia Bulgaria',
            date: {
                from: new Date('September 17, 2013 03:24:00'),
                to: new Date('September 17, 2015 03:24:00'),
            },
            description: ' ',
        },
    ],
    apps: new Array(14).fill('').map((i, i1) => {
        return { key: i1 + 1 }
    }),
}

const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
]

// class Tech extends React.Component {
const Tech = ({ tech, checked }) => {
    return (
        <Badge color={checked ? 'success' : 'primary'}>
            <strong>
                {tech} {}
            </strong>
        </Badge>
    )
}

class CV extends Component {
    constructor() {
        super()
        this.state = {
            checked: false,
            data: {
                cv: {
                    about: '',
                    frontend: '',
                    backend: '',
                    cover: [{ url: '' }],
                    ProfilePic: [{ url: '' }],
                    inverted: [{ url: '' }],
                },
            },
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(checked) {
        this.setState({ checked })
    }
    async componentWillMount() {
        const query = `{
             cv{
                Backend,
                Frontend,
                About,
                Cover{
                  url
                },
                ProfilePic{
                    url
                }
                Inverted{
                    url
                }
              }


          }`
        const data = await graph(query)
        console.log(data)

        this.setState({ data })
    }

    render() {
        const { classes } = this.props
        const imageClasses = classNames(
            classes.imgRaised,
            classes.imgRoundedCircle,
            classes.imgFluid
        )
        const { cv } = this.state.data
        const { checked } = this.state
        return (
            <div
                style={{
                    WebkitBoxShadow: 'none',
                    MozBoxShadow: 'none',
                    backgroundColor: checked ? '#27262C' : '#FFF',
                    color: checked ? '#FFF' : '#27262C',
                }}
            >
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Rudi CV</title>
                    <link
                        rel="stylesheet"
                        href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
                        integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
                        crossorigin="anonymous"
                    />
                </Helmet>

                <Parallax
                    height={50}
                    image={
                        checked
                            ? 'https://graphqldbone.herokuapp.com' +
                              cv.cover[0].url
                            : 'https://graphqldbone.herokuapp.com' +
                              cv.inverted[0].url
                    }
                ></Parallax>
                <div style={{ position: 'fixed', top: 10, right: 10 }}>
                    <Switch
                        offColor="#b2bec3"
                        onColor="#b2bec3"
                        uncheckedIcon={<div></div>}
                        offHandleColor="#2d3436"
                        onHandleColor="#dfe6e9"
                        checkedIcon={<div></div>}
                        onChange={this.handleChange}
                        checked={this.state.checked}
                    />
                </div>
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <div className={classes.profile}>
                            <div>
                                <img
                                    src={
                                        'https://graphqldbone.herokuapp.com' +
                                        cv.ProfilePic[0].url
                                    }
                                    alt="..."
                                    className={imageClasses}
                                />
                            </div>
                            <div className={classes.name}>
                                <h3
                                    style={{
                                        backgroundColor: checked
                                            ? '#27262C'
                                            : '#FFF',
                                    }}
                                >
                                    Radoslav Georgiev (Rudi)
                                </h3>
                                <h6>Full Stack DevOps</h6>
                                <IconButton
                                    color="transparent"
                                    className={classes.margin5}
                                    href="https://gitlab.com/arpecop/"
                                >
                                    <i
                                        className={`${classes.socials} fab fa-gitlab`}
                                    />
                                </IconButton>
                                <IconButton
                                    color="transparent"
                                    href="https://github.com/arpecop/"
                                    className={classes.margin5}
                                >
                                    <i
                                        className={`${classes.socials} fab fa-github`}
                                    />
                                </IconButton>
                                <IconButton
                                    color="transparent"
                                    className={classes.margin5}
                                    href="https://www.linkedin.com/in/kartinki/"
                                >
                                    <i
                                        className={`${classes.socials} fab fa-linkedin`}
                                    />
                                </IconButton>
                            </div>
                        </div>
                    </GridContainer>

                    <GridContainer justify="center">
                        <p style={{ fontSize: '1.1rem' }}>{cv.about}</p>

                        <Button color="primary" size="sm" simple>
                            <i className="material-icons">location_on</i>{' '}
                            Dobrich, Bulgaria
                        </Button>
                        <Button color="primary" size="sm" simple>
                            <i className="material-icons">date_range</i> 39
                            years old
                        </Button>
                        <Button color="primary" size="sm" simple>
                            <i className="material-icons">phone</i>{' '}
                            +359876358115
                        </Button>

                        <Button color="primary" size="sm" simple>
                            <i className="material-icons">email</i>{' '}
                            rudix.lab@gmail.com
                        </Button>
                    </GridContainer>

                    <h5>Skills</h5>
                    <span>
                        <div>
                            <h6>Frontend</h6>
                        </div>
                        {cv.frontend.split(',').map((prop, key) => (
                            <Tech tech={prop} key={key} checked={checked} />
                        ))}

                        <div>
                            <h6>Backend</h6>
                        </div>

                        {cv.backend.split(',').map((prop, key) => (
                            <Tech tech={prop} key={key} checked={checked} />
                        ))}
                    </span>

                    <h5>Work</h5>
                    {techs.jobs.map((job, key) => (
                        <div>
                            <div
                                style={{
                                    backgroundColor: checked
                                        ? '#2d3436'
                                        : '#9c27b0',
                                    padding: 20,
                                    color: '#FFF',
                                    marginBottom: 20,
                                }}
                            >
                                <span>
                                    <b>{job.title.split(',')[0]} </b>{' '}
                                    <b style={{ float: 'right' }}>
                                        {job.title.split(',')[1]}
                                    </b>
                                    <div style={{ fontSize: '90%' }}>
                                        <i
                                            className="material-icons"
                                            style={{ fontSize: '90%' }}
                                        >
                                            date_range
                                        </i>
                                        {job.date.from.getFullYear() +
                                            ' ' +
                                            monthNames[
                                                job.date.from.getMonth()
                                            ]}
                                        <span style={{ float: 'right' }}>
                                            <i
                                                className="material-icons"
                                                style={{ fontSize: '90%' }}
                                            >
                                                date_range
                                            </i>
                                            {job.date.to.getFullYear() +
                                                ' ' +
                                                monthNames[
                                                    job.date.to.getMonth()
                                                ]}
                                        </span>
                                    </div>
                                    <div>
                                        <strong>{job.description}</strong>
                                    </div>
                                </span>
                            </div>
                        </div>
                    ))}
                </div>
                <Footer />
            </div>
        )
    }
}

export default withStyles(profilePageStyle)(CV)
