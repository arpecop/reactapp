import env from './env.js'
import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Router, Route, Switch } from 'react-router'
import registerMyServiceWorker from './registerServiceWorker'
import 'assets/scss/material-kit-react.css'

// import Components from "./views/Components/Components.jsx";
import NewsBg from './views/NewsBg/NewsBg.jsx'
import CV from './views/CV/CV.jsx'
import VijdamTe from './views/VijdamTe/VijdamTe.jsx'
import Corona from './views/Corona/Corona.jsx'
import LoggedIn from './views/VijdamTe/LoggedIn.jsx'
import AddItem from './views/VijdamTe/AddItem.jsx'
registerMyServiceWorker()

const indexRoutes = {
    newsbg: [
        // { path: '/login', name: 'LoginPage', component: LoginPage },
        { path: '/', name: 'Components', component: NewsBg },
    ],
    rudi: [
        { path: '/', name: 'Components', component: CV, exact: true },
        // { path: "/test", name: "Components", component: Components }
    ],
    corona: [
        { path: '/', name: 'Components', component: Corona, exact: true },
        // { path: "/test", name: "Components", component: Components }
    ],
    vijdamte: [
        {
            path: '/add/:lat/:lng',
            name: 'Components',
            component: AddItem,
            exact: true,
        },
        { path: '/:id', name: 'Components', component: LoggedIn, exact: true },
        { path: '/', name: 'Components', component: VijdamTe, exact: true },
    ],
}

const hist = createBrowserHistory()

ReactDOM.render(
    <Router history={hist} basename={process.env.PUBLIC_URL}>
        <Switch>
            {indexRoutes[process.env.SITE || env.SITE].map((prop, key) => (
                <Route path={prop.path} key={key} component={prop.component} />
            ))}
        </Switch>
    </Router>,
    document.getElementById('root')
)
